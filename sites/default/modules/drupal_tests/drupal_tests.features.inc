<?php
/**
 * @file
 * drupal_tests.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drupal_tests_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function drupal_tests_node_info() {
  $items = array(
    'pagina' => array(
      'name' => t('Página'),
      'base' => 'node_content',
      'description' => t('Páginas de conteúdo estático.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
