<?php
/**
 * @file
 * drupal_tests.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function drupal_tests_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'Sobre',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '13e6e048-9469-4dc1-beaa-4b81857d250f',
  'type' => 'pagina',
  'language' => 'en',
  'created' => 1376603227,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'e72e3c84-1461-4c2c-9b79-9eff96cf2a4a',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'Conteúdo da página Sobre...',
        'summary' => '',
        'format' => 'plain_text',
        'safe_value' => '<p>Conteúdo da página Sobre...</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2013-08-15 18:47:07 -0300',
);
  return $nodes;
}
